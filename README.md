<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# Continuous Integration with MCUBoot

This project provides a GitLab CI/CD pipeline building MCUBoot (the Zephyr
fork) in the context of Zephyr for a specific board and programs the board
using Segger J-Link. 

## Early Access

This project is under development and is in early stages of evolution. It may
change rapidly and may not be in a useful state at a particular point in time.

## Relationships and dependencies

This project depends on the following projects:

- Zephyr RTOS
- MCUBoot (in particular the fork at https://github.com/zyga/mcuboot)
- zephyr-ci-pipelines from https://gitlab.com/zygoon/zephyr-ci-pipelines
- gitlab-lxd-executor from https://gitlab.com/zygoon/gitlab-lxd-executor

The project is using the Nordic nRF52840 DK board for testing, as this board
includes a Segger J-Link OB (On-Board) debugger, allowing us to replace the
bootloader without any effort. You can find more about this board at 
https://www.nordicsemi.com/Products/Development-hardware/nrf52840-dk

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making it
easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
